import PySimpleGUI as sg  
from sqlite_demo import DB
from Book import Book

db = DB()

import PySimpleGUI as sg

# Define the window's contents
start_layout = [[sg.Button('Search Book'), sg.Button('Add Book'), sg.Button('Quit')]]

add_book_layout = [[sg.Text("ID")],
            [sg.Input(key='-ID-')],
            [sg.Text("Title")],
            [sg.Input(key='-TITLE-')],
            [sg.Text("Author")],
            [sg.Input(key='-AUTHOR-')],
            [sg.Text("Status")],
            [sg.Input(key='-STATUS-')],
            [sg.Text(key='-ADDRESULT-')],
            [sg.Button('Save'), sg.Button('Back', key='-BACK1-')]]

search_layout = [[sg.Text("Search a book by")],
            [sg.Radio("Title", "RADIO1", key="-SEARCHTITLE-", default=True), sg.Radio("Author", "RADIO1", key="-SEARCHAUTHOR-", default=False)],
            [sg.Input(key="-SEARCHTERM-")],
            [sg.Text(key='-SEARCHRESULT-')],
            [sg.Button("Search"), sg.Button('Back', key='-BACK2-')]]

issue_layout = [[sg.Text("Book ID: ")],
                [sg.Text("Issued To: "), sg.Input(key="-ISSUEDTO-")],
                [sg.Button("Save", key = "-SAVEISSUE-"), sg.Button("Quit", key = "-QUITISSUE-")]]


layout = [[sg.Column(start_layout, visible = True, key = "-COL1-"), 
        sg.Column(add_book_layout, visible = False, key= "-COL2-"),
        sg.Column(search_layout, visible = False, key= "-COL3-"),
        sg.Column(issue_layout, visible = False, key= "-COL4-")]]


window = sg.Window('Library', layout, size=(500,500))

# Display and interact with the Window using an Event Loop
while True:
    event, values = window.read()
    
    # Quit Button Event
    if event == sg.WINDOW_CLOSED or event == 'Quit':
        break
    # Add Book Button Event
    elif event == "Add Book":
        window[f'-COL2-'].update(visible=True)
        window[f'-COL1-'].update(visible=False)
        window[f'-COL3-'].update(visible=False)
        window[f'-COL4-'].update(visible=False)
    # Search Book Button Event
    elif event == "Search Book":
        window[f'-COL1-'].update(visible=False)
        window[f'-COL2-'].update(visible=False)
        window[f'-COL3-'].update(visible=True)
        window[f'-COL4-'].update(visible=False)
    # Back Buttons Event
    elif event == "-BACK1-" or event == "-BACK2-" or event == "-QUITISSUE-":
        window[f'-COL2-'].update(visible=False)
        window[f'-COL1-'].update(visible=True)
        window[f'-COL3-'].update(visible=False)
        window[f'-COL4-'].update(visible=False)
        # reset search and add results outputs 
        window["-SEARCHRESULT-"].update("")
        window["-ADDRESULT-"].update("")

    # Search Button Event
    elif event == "Search":
        
        if values["-SEARCHTITLE-"] == True:
            title = values["-SEARCHTERM-"]
            if title != "":
                books = db.get_book_by_title(title)
                res = ""

                if len(books) == 0:
                    window["-SEARCHRESULT-"].update("Book \"" + title + "\" not found.")
                else:
                    for book in books:
                        res += "Title: " + book[1] + "\n" + "Author: " + book[2] + "\n" + "Status: " + book[3] + "\n\n"
                    
                    window["-SEARCHRESULT-"].update(res)            
            else:
                window["-SEARCHRESULT-"].update("Please enter a title")
        
        else:
            author = values["-SEARCHTERM-"]
            if author != "":
                books = db.get_book_by_author(author)
                res = ""
                if len(books) == 0:
                    window["-SEARCHRESULT-"].update("Author \"" + author + "\" not found.")
                else:
                    for book in books:
                        res += "Title: " + book[1] + "\n" + "Author: " + book[2] + "\n" + "Status: " + book[3] + "\n\n"
                    
                    window["-SEARCHRESULT-"].update(res)
            else:
                window["-SEARCHRESULT-"].update("Please enter an author")
          
        # reset search inputs
        window["-SEARCHTERM-"].update("")
    
    # Issue Button Event
    elif event == "Issue":
        window[f'-COL1-'].update(visible=False)
        window[f'-COL2-'].update(visible=False)
        window[f'-COL3-'].update(visible=False)
        window[f'-COL4-'].update(visible=True)
        print("issued")
    
    # Return Button Event
    elif event == "Return":
        print("returned")

    # Save Button Event
    elif event == "Save":
        book = Book(values["-ID-"], values["-TITLE-"], values["-AUTHOR-"], values["-STATUS-"])
        window["-ADDRESULT-"].update("Title: " + book.title 
                                + "\n" + "Author: " + book.author 
                                + "\n" + "Status: " + book.status)
        db.insert_book(book)
        # reset search inputs
        window["-ID-"].update("")
        window["-TITLE-"].update("")
        window["-AUTHOR-"].update("")
        window["-STATUS-"].update("")

window.close()

