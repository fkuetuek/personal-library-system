import sqlite3
from Book import Book

class DB:

    def __init__(self):
        self.conn = sqlite3.connect("../db/library.db")
        self.c = self.conn.cursor()


    def insert_book(self, book):
        with self.conn:
            self.c.execute("INSERT INTO books VALUES (:id, :title, :author, :status)", {"id": book.id, "title": book.title, "author": book.author, "status": book.status})


    def get_book_by_title(self, title):
        query = "SELECT * FROM books WHERE title LIKE " + "'%" + title + "%'"
        self.c.execute(query, {"title": title})
        # self.c.execute("SELECT * FROM books WHERE title MATCH :title", {"title": title})
        return self.c.fetchall()

    def get_book_by_author(self, author):
        query = "SELECT * FROM books WHERE author LIKE " + "'%" + author + "%'"
        self.c.execute(query, {"author": author})
        # self.c.execute("SELECT * FROM books WHERE author MATCH :author", {"author": author})

        return self.c.fetchall()

    def update_status(self, book, status):
        with self.conn:
            self.c.execute("""UPDATE books SET status = :status
                        WHERE title = :title AND author = :author""",
                        {"title": book.title, "author": book.author, "status": status})
                        

    def delete_book(self, book):
        with self.conn:
            self.c.execute("DELETE from books WHERE title = :title AND author = :author", 
            {"title": book.title, "author": book.author})


    def search_book(self):
        pass


    def close_connection(self):
        self.conn.close()    


# book_1 = Book(31, "The Help", "Kathryn Stockett", "available")



# book = search_book()

#update_status(book_1, "available")

#delete_book(book_1)

#c.execute("SELECT * FROM books")

#conn.commit()
# print(book)


###### Creating database in memory
# Create a memory on ram instead of using a .db file
# conn = sqlite3.connect(":memory:")

# conn = sqlite3.connect("../db/library.db")

# c = conn.cursor()

# c.execute("""CREATE TABLE books (
#             ID integer,
#             TITLE text,
#             AUTHOR text,
#             STATUS text
#             )""")

# book_1 = Book(31, "The Help", "Kathryn Stockett", "available")

# Two ways to insert data to database
# c.execute("INSERT INTO books VALUES (?, ?, ?, ?)", (book_1.id, book_1.title, book_1.author, book_1.status))
# conn.commit()

# c.execute("INSERT INTO books VALUES (:ID, :TITLE, :AUTHOR, :STATUS)", {"ID": book_1.id, "TITLE": book_1.title, "AUTHOR": book_1.author, "STATUS": book_1.status})
# conn.commit()

# c.execute("SELECT * FROM books WHERE ID=?", (19,))

# print(c.fetchall())

# c.execute("SELECT * FROM books WHERE AUTHOR=:AUTHOR", {"AUTHOR": "J.K. Rowling"})

# print(c.fetchall())

# conn.close()
